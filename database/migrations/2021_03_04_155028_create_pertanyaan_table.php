<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id_pertanyaan');
            $table->string('judul');
            $table->date('tgl_dibuat');
            $table->date('tgl_diperbaharui');
            
            $table->unsignedBigInteger('id_users');
            $table->foreign('id_users')->references('id_pertanyaan')->on('users');

            $table->unsignedBigInteger('id_jawaban');
            $table->foreign('id_jawaban')->references('id_pertanyaan')->on('jawaban');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pertanyaan');
    }
}
