<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarPertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar-pertanyaan', function (Blueprint $table) {
            $table->bigIncrements('id_komentar_pertanyaan');
            $table->string('isi');
            $table->date('tgl_dibuat');

            $table->unsignedBigInteger('id_users');
            $table->foreign('id_users')->references('id_komentar_pertanyaan')->on('users');

            $table->unsignedBigInteger('id_pertanyaan');
            $table->foreign('id_pertanyaan')->references('id_komentar_pertanyaan')->on('pertanyaan');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_pertanyaan');
    }
}
