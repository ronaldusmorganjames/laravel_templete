<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_jawaban', function (Blueprint $table) {
            $table->bigIncrements('id_komentar_jawaban');
            $table->string('isi');
            $table->date('tgl_dibuat');

            $table->unsignedBigInteger('id_users');
            $table->foreign('id_users')->references('id_komentar_jawaban')->on('users');

            $table->unsignedBigInteger('id_jawaban');
            $table->foreign('id_jawaban')->references('id_komentar_jawaban')->on('jawaban');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_jawaban');
    }
}
