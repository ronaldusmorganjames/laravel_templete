<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikeJawabanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like-dislike-jawaban', function (Blueprint $table) {
            
            $table->bigIncrements('id_like_dislike_jwb');

            $table->unsignedBigInteger('id_users');
            $table->foreign('id_users')->references('id_like_dislike_jwb')->on('users');

            $table->unsignedBigInteger('id_jawaban');
            $table->foreign('id_jawaban')->references('id_like_dislike_jwb')->on('jawaban');

            $table->integer('poin');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like-dislike-jawaban');
    }
}
