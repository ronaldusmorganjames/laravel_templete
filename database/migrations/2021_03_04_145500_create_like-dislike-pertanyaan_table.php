<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLikeDislikePertanyaanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('like-dislike-pertanyaan', function (Blueprint $table) {
            
            $table->bigIncrements('id_like_dislike_prt');

            $table->unsignedBigInteger('id_users');
            $table->foreign('id_users')->references('id_like_dislike_prt')->on('users');

            $table->unsignedBigInteger('id_pertanyaan');
            $table->foreign('id_pertanyaan')->references('id_like_dislike_prt')->on('pertanyaan');

            $table->integer('poin');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('like-dislike-pertanyaan');
    }
}
